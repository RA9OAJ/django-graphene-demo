# Django Graphene demo

**Файл с БД в пример не входит!**
Поэтому выполняем стандартные действия при разворачивании проекта

```bash
# Загружаем все пакеты
$ pip install -r requirements.txt

# Применяем миграции, файл БД sqlite создастся автоматом
$ python3 manage.py migrate
или
$ python manage.py migrate


# Запускаем сервер разработки
$ python3 manage.py runserver
или
$ python manage.py runserver
```
Наслаждаемся редактором запросов GraphQL по адресу http://127.0.0.1/api/v1.0/

