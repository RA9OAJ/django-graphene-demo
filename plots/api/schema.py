# Классы для сопряжения с моделями Django
from graphene_django import DjangoObjectType, DjangoConnectionField
# Модуль-ядро Graphene
import graphene

# Для демонстрации используем встроенную модель пользователя
from django.contrib.auth.models import User


# Описываем новый класс Django объекта (узел)
class APIUser(DjangoObjectType): # название класса произвольное
    class Meta:
        model = User # Указываем модель
        interfaces = (graphene.relay.Node,) # указываем реализуемые интерфейсы


# А это уже класс, каждое поле которого
# это узел запроса. В данном случае узел
# называется user и будет выдавать все записи
# модели User
# Результирйщий класс-схема всегда наследуется от graphene.ObjectType
class PlotsQuery(graphene.ObjectType):
    # Через тип DjangoConnectionField подключаем описанную выше модель
    users = DjangoConnectionField(APIUser)
