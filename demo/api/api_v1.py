# Импортируем Grapheme
import graphene
# импортируем результируйщий класс с узлами
# из приложения plots
from plots.api.schema import PlotsQuery

# Этот класс создан для удобства и связывания всех
# узлов, в данном случае он наследуется от PlotsQuery
# Если необходимо подключить больше узлов из разных приложений,
# то следует испорльзовать множественное наследование:
# GlobalQuery(PlotsQuery, FooQuery, BarQuery)
class GlobalQuery(PlotsQuery):
    # сам класс пустой, а лишь наследует поля и методы
    # родителей
    pass


# а вот это уже точка вхождения в API
# переменная типа Schema, где в параметр конструктора
# query передаем в качестве аргумента наш глобальный класс
# GlobalQuery
schema = graphene.Schema(query=GlobalQuery)
